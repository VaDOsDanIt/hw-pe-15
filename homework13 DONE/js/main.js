const modalInput = document.querySelector('.modal-input');
const input = modalInput.querySelectorAll('input');
const signIn = document.querySelector('.signIn');
const lineOr = document.querySelector('.line-or > span');
const check = document.querySelector('.modal-footer a');
const modal = document.querySelector('.modal');
const button = document.querySelector('.dark-theme');
button.className = 'button-theme';
button.innerText = 'Dark';

button.addEventListener('click', darkTheme);

if(localStorage.getItem('theme') !== undefined) {
    button.innerText = localStorage.getItem('theme');
    darkTheme();
}

function darkTheme() {

    if (button.innerText === 'Dark') {

        for (let i = 0; i < input.length; i++) {
            input[i].style.backgroundColor = '#3f3f3f';
            input[i].style.color = '#eaeaea';
        }
        modal.style.backgroundColor = '#2f2f2f';
        signIn.style.color = '#eaeaea';
        lineOr.style.backgroundColor = '#2f2f2f';
        check.style.color = 'rgb(117, 117, 108)';

        localStorage.setItem('theme', button.innerText);
        button.innerText = 'Light';



    } else {
        for (let i = 0; i < input.length; i++) {
            input[i].style.backgroundColor = '#fff';
            input[i].style.color = '#757575';
        }
        modal.style.backgroundColor = '#fff';
        signIn.style.color = '#434343';
        lineOr.style.backgroundColor = '#fff';
        check.style.color = '#434343';

        localStorage.setItem('theme', button.innerText);
        button.innerText = 'Dark';



    }
}